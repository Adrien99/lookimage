<?php

namespace App\DataFixtures;

use App\Entity\Picture;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Mmo\Faker\PicsumProvider;
use Faker;
use Symfony\Component\HttpFoundation\File\File;

class PictureFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * Permet de dire a notre fixtures, si elle dépend d'autres fixtures
     * afin de ne pas avoir d'erreurs lors de l'utlisation des "getReference()"
     * L'ordres des fixtures ne sera plus par ordre alphabétique, mais par ordre de dépendances.
     */
    public function getDependencies()
    {
        return [
            CategorieFixtures::class,
            UsersFixtures::class
        ];
    }
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');

        // on ajoute le bundle Mmo faker image a faker
        $faker->addProvider(new PicsumProvider($faker));

        for ($i = 0; $i <= 100; $i++) {

            // Spécifier le chemin du dossier d'upload
            // les deux autres paramêtre sont a la hauteur et la largeur de l'image a récupérer
            $image = $faker->picsum('./public/uploads/images/photos', random_int(1152, 2312), random_int(864, 1736));

            // Récupération dune référece aléatoirement.
            // on récupère un objet de l'entité Categorie généré dans le fchier CategorieFixtures
            // au nom choisi ors de l'enregitrement dans les réféences
            $category = $this->getReference('category_'. random_int(0, 10));

            // Récupère ue référence utilisateur aléatoirement
            $user = $this->getReference('user_'. random_int(0, 10));

            $picture = new Picture();
            $picture->setDescription($faker->sentence(26));
            $picture->setTags($faker->word);
            $picture->setCreatedAt($faker->dateTimeBetween('-4 years',));
            $picture->setUpdatedAt($faker->dateTimeBetween('-2 years',));
            $picture->setCategory($category);
            $picture->setUser($user);

            // Gestion de l'image
            // A insérer en haut de page : use Symfony\Component\HttpFoundation\File\File;
            $picture->setImageFile(new File($image));

            // str_replace() permet de chercher un morceau de caractères dans une chaîne de caractères et nous pouvons le
            // remplacer par ce que l'on souhaite, dans notre cas de figure, par rien
            // 1 argument : chaîne à rechercher
            // 2 argument : par quoi remplacer le 1er argument
            // 3 argument : où effectuer la rechercher ?
            $picture->setImage(str_replace('./public/uploads/images/photos\\', '', $image));


            $manager->persist($picture);
        }


        $manager->flush();
    }
}
