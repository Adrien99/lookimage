<?php

namespace App\DataFixtures;

use App\Entity\Categorie;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;
use Symfony\Component\String\Slugger\AsciiSlugger;

class CategorieFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
// instancier Faker
$faker = Faker\Factory::create('fr_FR');

// Création d'une boucle for() pour choisir le nombre d'éléments
// allant a la BDD
for($i = 0; $i<= 10; $i++){
    $category = new Categorie();
    $category->setName($faker->colorName);
    $category->setSlug((new AsciiSlugger())->slug(strtolower($faker->colorName)));

// enregistre l'objet dans une référence. CaD que l'on pourra utiliser les objet
// enregistré dans une autre fixture afin de pouvoir effectuer des relations 
// de table
// le premier paramêtre est un nom qui se doi d'être unique 
// le second paramêtre est l'objet qui sera lié a se nom
$this->addReference('category_'.$i,$category);

    // garde côté en attendant l'éxécution des requêtes
$manager->persist($category);
}

        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }
}
