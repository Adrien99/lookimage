<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Picture;
use App\Form\CategoryType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\AsciiSlugger;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin/categorie", name="admin")
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {

        $query = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

        $page = $request->query->getInt('page', 1);
        $categories = $paginator->paginate(
            $query,
            $page === 0 ? 1 : $page,
            20
        );

        return $this->render('admin/index.html.twig', [
            'categories' => $categories,
        ]);
    }

    /**
     *  @Route("/admin/categorie/new", name="admin_categorie_new")
     */
    public function new (Request $request)
    {
        $category = new Categorie();

        $formNewCategory = $this->createForm(CategoryType::class, $category);
        $formNewCategory->handleRequest($request);

        if($formNewCategory->isSubmitted() && $formNewCategory->isValid()) {
            $category->setSlug((new AsciiSlugger())->slug(strtolower($category->getName())));

            // insertion en BDD
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($category);
            $doctrine->flush();

            $this->addFlash('success', 'La catégorie à bien eté créée');

            return $this->redirectToRoute('admin');

        }

        return $this->render('admin/new.html.twig', [
            'formNewCategory' => $formNewCategory->createView()
        ]);
    }
}
